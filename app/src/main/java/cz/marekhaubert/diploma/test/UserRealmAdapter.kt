package cz.marekhaubert.diploma.test

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.User
import cz.marekhaubert.diploma.UserRealm
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_basic_adapter.view.*

/**
 * Created by Marek Haubert on 30.07.2017.
 */
class UserRealmAdapter(val context: Context, private val items: OrderedRealmCollection<UserRealm>, val onClicklistener: (UserRealm) -> Unit, val onLongClicklistener: (UserRealm) -> Unit) : RealmRecyclerViewAdapter<UserRealm, UserRealmAdapter.ViewHolder>(items, true) {

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_basic_adapter, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], onClicklistener, onLongClicklistener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: UserRealm, onClicklistener: (UserRealm) -> Unit, onLongClicklistener: (UserRealm) -> Unit) = with(itemView) {
            txtName.text = user.name
            txtEmail.text = user.email
            txtPhone.text = user.phone
            Glide.with(context).load("http://api.adorable.io/avatar/${user.imageId}").diskCacheStrategy(DiskCacheStrategy.ALL).into(imgAvatar)
            setOnClickListener({ onClicklistener(user) })
            setOnLongClickListener({ onLongClicklistener(user);true })
        }

    }
}