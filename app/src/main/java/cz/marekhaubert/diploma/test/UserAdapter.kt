package cz.marekhaubert.diploma.test

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.User
import cz.marekhaubert.diploma.UserRealm
import kotlinx.android.synthetic.main.item_basic_adapter.view.*

/**
 * Created by Marek on 13.10.2017.
 */
class UserAdapter(val context: Context, private val items: List<User>, val onClicklistener: (User) -> Unit, val onLongClicklistener: (User) -> Unit) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_basic_adapter, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], onClicklistener, onLongClicklistener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User, onClicklistener: (User) -> Unit, onLongClicklistener: (User) -> Unit) = with(itemView) {
            txtName.text = user.name
            txtEmail.text = user.email
            txtPhone.text = user.phone
            Glide.with(context).load("http://api.adorable.io/avatar/${user.imageId}").diskCacheStrategy(DiskCacheStrategy.ALL).into(imgAvatar)
            setOnClickListener({ onClicklistener(user) })
            setOnLongClickListener({ onLongClicklistener(user);true })
        }

    }
}