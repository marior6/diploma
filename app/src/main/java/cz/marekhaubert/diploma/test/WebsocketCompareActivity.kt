package cz.marekhaubert.diploma.test

import android.util.Log
import com.google.gson.GsonBuilder
import cz.marekhaubert.diploma.IUserItem
import cz.marekhaubert.diploma.User
import cz.marekhaubert.diploma.WEBSOCKET_SERVER_URL
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_comparison.*
import org.java_websocket.WebSocket
import org.jetbrains.anko.toast
import ua.naiksoftware.stomp.LifecycleEvent
import ua.naiksoftware.stomp.Stomp

/**
 * Created by Marek Haubert on 16.10.2017.
 */

class WebsocketCompareActivity : BaseCompareActivity() {

    private val TAG = WebsocketCompareActivity::class.java.name

    //private val stompClient = Stomp.over(WebSocket::class.java, WEBSOCKET_SERVER_URL)
    private val gson = GsonBuilder().create()
    var restDisposable: Disposable? = null

    override fun onDestroy() {
        stompClient.disconnect()
        if (restDisposable != null) restDisposable!!.dispose()
        super.onDestroy()
    }

    enum class Action(private val actionId: kotlin.Int) {
        CREATE(1), EDIT(2), DELETE(3);

        fun getActionId() = actionId
    }

    // Inicializace STOMP komunikace prostřednictvím websocketového připojení
    private val stompClient = Stomp.over(WebSocket::class.java, WEBSOCKET_SERVER_URL)
    val userList = mutableListOf<User>()

    /**
     * Provádí inicializaci spojení se serverovou aplikací.
     */
    override fun initClient() {
        // inicializace seznamu uživatelů z databáze
        loadDataToRecyclerView()

        // monitoring stavu připojení k serveru
        stompClient.lifecycle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { lifecycleEvent ->
                    when (lifecycleEvent.type) {
                        LifecycleEvent.Type.OPENED -> toast("Stomp connection opened")
                        LifecycleEvent.Type.ERROR -> toast("Stomp connection error")
                        LifecycleEvent.Type.CLOSED -> toast("Stomp connection closed")
                        else -> toast("Stomp unknown error")
                    }
                }

        // příjem zpráv z topicu označeného jako 'users'
        stompClient.topic("/topic/users")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ topicMessage ->
                    // deserializace příchozí zprávy z formátu JSON do objektu User
                    val receivedUser = gson.fromJson(topicMessage.payload, User::class.java)
                    when (receivedUser.actionId) {
                    // vytvoření uživatele
                        Action.CREATE.getActionId() -> {
                            userList.add(receivedUser)
                            recyclerUser.adapter.notifyDataSetChanged()
                        }
                    // editace stávajícího uživatele
                        Action.EDIT.getActionId() -> {
                            val user = userList.find { user -> user.id.equals(receivedUser.id) }
                            userList[userList.indexOf(user)] = receivedUser
                            recyclerUser.adapter.notifyDataSetChanged()
                        }
                    // odstranění uživatele
                        Action.DELETE.getActionId() -> {
                            val user = userList.find { user -> user.id.equals(receivedUser.id) }
                            userList.remove(user)
                            recyclerUser.adapter.notifyDataSetChanged()
                        }
                    }
                })

        stompClient.connect()
    }

    /**
     * Provádí inicializaci seznamu uživatelů z databáze.
     */
    private fun loadDataToRecyclerView() {
        // vytvoření adaptéru pro zobrazení načtených záznamů
        recyclerUser.adapter = UserAdapter(this, userList,
                // po kliknutí na záznam jsou data uživatele vepsána do editačních polí k editaci uživatele
                { userToEdit -> setEditedUserValuesToViews(userToEdit) },
                // po dlouhém podržení záznamu je uživatel z databáze odstraněn
                { userToDelete -> deleteUser(userToDelete) }
        )

        // prvotní načtení seznamu uživatelů ze serveru při spuštění Activity prostřednictvím REST architektury
        restDisposable = RestClient.getRestInstance()?.exampleRepository
                ?.getAllUsers()
                ?.unsubscribeOn(Schedulers.newThread())
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    userList.addAll(it)
                    recyclerUser.adapter.notifyDataSetChanged()
                }, { it.printStackTrace() })
    }

    /**
     * Vytváří nového uživatele v Realm databázi.
     */
    override fun createUser(selectedAvatarPositionView: Int, name: String, email: String, phone: String) {
        sendStompMessage(Action.CREATE.getActionId(), User(0L, name, email, phone, selectedAvatarPositionView, 0))
    }

    /**
     * Provádí aktualizaci uživatele v Realm databázi.
     */
    override fun updateUser(editedItemId: Long, selectedAvatarPositionView: Int, name: String, email: String, phone: String) {
        sendStompMessage(Action.EDIT.getActionId(), User(editedItemId, name, email, phone, selectedAvatarPositionView, 0))
    }

    /**
     * Odstraňuje uživatele z databáze.
     * @param userToDelete Objekt uživatele určeného z vymazání.
     */
    override fun deleteUser(userToDelete: IUserItem) {
        sendStompMessage(Action.DELETE.getActionId(), userToDelete)
    }

    /**
     * Provádí odeslání požadavku na server prostřednictvím protokolu STOMP.
     * @param actionId Identifikátor akce.
     * @param user Objekt uživatele nad kterým má být akce provedena.
     */
    private fun sendStompMessage(actionId: Int, user: IUserItem) {
        stompClient.send("/topic/user/$actionId", gson.toJson(user))
                .unsubscribeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Log.d(TAG, "Stomp send successfully") }) { throwable ->
                    Log.e(TAG, "Error stomp send", throwable)
                    throwable.message?.let { toast(it) }
                }
    }

    override fun getItemById(itemId: Long): User? = userList.find { user -> user.id == itemId }
}
