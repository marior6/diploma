package cz.marekhaubert.diploma.test

import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import cz.marekhaubert.diploma.IUserItem
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.base.BaseActivity
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_comparison.*

/**
 * Created by Marek Haubert on 16.10.2017.
 */

abstract class BaseCompareActivity : BaseActivity() {

    lateinit var avatarViewList: List<CircleImageView>
    var selectedAvatarPositionView = 0
    var editedItemId = -1L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comparison)
        setSupportActionBar(toolbar)
        initViews()
        initClient()
    }

    private fun initViews() {
        recyclerUser.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerUser.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        initAvatarViews()
        clearEditableViews()
        fabSend.setOnClickListener {
            if (validateForm()) {
                if (editedItemId == -1L) {
                    createUser(selectedAvatarPositionView, editName.text.toString(), editEmail.text.toString(), editPhone.text.toString())
                } else {
                    updateUser(editedItemId, selectedAvatarPositionView, editName.text.toString(), editEmail.text.toString(), editPhone.text.toString())
                }
                clearEditableViews()
            }
        }
        imgCancel.setOnClickListener { clearEditableViews() }
    }

    private fun validateForm(): Boolean {
        if (editName.text.isEmpty()) {
            editName.setError(getString(R.string.mandatory_field))
        } else if (editEmail.text.isEmpty()) {
            editEmail.setError(getString(R.string.mandatory_field))
        } else if (editPhone.text.isEmpty()) {
            editPhone.setError(getString(R.string.mandatory_field))
        } else {
            return true
        }
        return false
    }

    private fun clearEditableViews() {
        editedItemId = -1L
        txtHeader.setText(R.string.add_user)
        imgCancel.visibility = View.GONE
        editName.setText("")
        editEmail.setText("")
        editPhone.setText("")
    }

    protected fun setEditedUserValuesToViews(user: IUserItem) {
        txtHeader.setText(R.string.edit_user)
        imgCancel.visibility = View.VISIBLE
        editedItemId = user.id
        editName.setText(user.name)
        editEmail.setText(user.email)
        editPhone.setText(user.phone)
        updateSelectedAvatar(avatarViewList[user.imageId])
    }

    private fun initAvatarViews() {
        avatarViewList = listOf(imgAvatar1, imgAvatar2, imgAvatar3, imgAvatar4, imgAvatar5, imgAvatar6, imgAvatar7)
        avatarViewList.forEach {
            it.borderColor = Color.WHITE
            Glide.with(this@BaseCompareActivity).load("http://api.adorable.io/avatar/${avatarViewList.indexOf(it)}").into(it)
            it.setOnClickListener({ updateSelectedAvatar(it) })
        }
        avatarViewList[selectedAvatarPositionView].borderColor = Color.BLACK
    }

    private fun updateSelectedAvatar(it: View) {
        avatarViewList.forEach { it.borderColor = Color.WHITE }
        (it as CircleImageView).borderColor = Color.BLACK
        selectedAvatarPositionView = avatarViewList.indexOf(it)
    }

    abstract fun initClient()
    abstract fun createUser(selectedAvatarPositionView: Int, name: String, email: String, phone: String)
    abstract fun updateUser(editedItemId: Long, selectedAvatarPositionView: Int, name: String, email: String, phone: String)
    abstract fun deleteUser(userToDelete: IUserItem)
    abstract fun getItemById(itemId: Long): IUserItem?

}