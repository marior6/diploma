package cz.marekhaubert.diploma.test

import cz.marekhaubert.diploma.REST_SERVER_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.google.gson.GsonBuilder
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.OkHttpClient


/**
 * Created by Marek Haubert on 13.10.2017.
 */
class RestClient private constructor() {

    val exampleRepository: RestInterface

    init {
        val retrofit = Retrofit.Builder().baseUrl(REST_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        exampleRepository = retrofit.create<RestInterface>(RestInterface::class.java)
    }

    companion object {
        var instance: RestClient? = null
        private val lock = Any()

        fun getRestInstance(): RestClient? {
            var instance = RestClient.instance
            if (instance == null) {
                synchronized(lock) {
                    instance = RestClient.instance
                    if (instance == null) {
                        instance = RestClient()
                        RestClient.instance = instance
                    }
                }
            }
            return instance
        }
    }
}
