package cz.marekhaubert.diploma.test

import cz.marekhaubert.diploma.User
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Marek Haubert on 13.10.2017.
 */
interface RestInterface {

    @GET(".")
    fun getAllUsers(): Observable<List<User>>
}
