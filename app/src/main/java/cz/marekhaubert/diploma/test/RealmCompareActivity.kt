package cz.marekhaubert.diploma.test

import cz.marekhaubert.diploma.IUserItem
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.UserRealm
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_comparison.*
import org.jetbrains.anko.toast

/**
 * Created by Marek Haubert on 16.10.2017.
 */
class RealmCompareActivity : BaseCompareActivity() {

    private val TAG = RealmCompareActivity::class.java.name

    // Inicializace Realm databáze
    private val realm: Realm = Realm.getDefaultInstance()

    override fun onDestroy() {
        realm.removeAllChangeListeners()
        if (realm.isInTransaction) {
            realm.cancelTransaction()
        }
        realm.close()
        super.onDestroy()
    }

    /**
     * Provádí inicializaci seznamu uživatelů z Realm databáze.
     */
    override fun initClient() {
        // vyčtení seznamu uživatelů z databáze
        val userList = realm.where(UserRealm::class.java).findAllAsync()
        // vytvoření adaptéru pro zobrazení načtených záznamů
        recyclerUser.adapter = UserRealmAdapter(this, userList,
                // po kliknutí na záznam jsou data uživatele vepsána do editačních polí k editaci uživatele
                { userToEdit -> if (userToEdit.isValid) setEditedUserValuesToViews(userToEdit) },
                // po dlouhém podržení záznamu je uživatel z databáze odstraněn
                { userToDelete -> if (userToDelete.isValid) deleteUser(userToDelete) }
        )
    }

    /**
     * Vytváří nového uživatele v Realm databázi.
     */
    override fun createUser(selectedAvatarPositionView: Int, name: String, email: String, phone: String) {
        realm.executeTransaction { realm.copyToRealmOrUpdate(UserRealm(getNextUserId(), name, email, phone, selectedAvatarPositionView)) }
        // zobrazení informační hlášky uživateli
        toast(R.string.user_added)
    }

    /**
     * Provádí aktualizaci uživatele v Realm databázi.
     */
    override fun updateUser(editedItemId: Long, selectedAvatarPositionView: Int, name: String, email: String, phone: String) {
        realm.executeTransaction { realm.copyToRealmOrUpdate(UserRealm(editedItemId, name, email, phone, selectedAvatarPositionView)) }
        // zobrazení informační hlášky uživateli
        toast(R.string.user_updated)
    }

    /**
     * Odstraňuje uživatele z databáze.
     * @param userToDelete Objekt uživatele určeného z vymazání.
     */
    override fun deleteUser(userToDelete: IUserItem) {
        realm.executeTransaction { (userToDelete as UserRealm).deleteFromRealm() }
        // zobrazení informační hlášky uživateli
        toast(R.string.user_deleted)
    }

    override fun getItemById(itemId: Long): IUserItem? = realm.where(UserRealm::class.java).equalTo("id", itemId).findFirst()

    private fun getNextUserId(): Long {
        var nextId = 0L
        try {
            nextId = realm.where(UserRealm::class.java).max("id").toLong() + 1
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return nextId
    }
}
