package cz.marekhaubert.diploma.pubnub

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import com.bumptech.glide.Glide
import cz.marekhaubert.diploma.ChatMessage
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.getFormattedTime
import kotlinx.android.synthetic.main.item_chat_message.view.*
import org.joda.time.DateTime
import org.joda.time.Hours
import org.joda.time.Minutes
import org.joda.time.format.DateTimeFormat
import java.util.*

/**
 * Created by Marek Haubert on 29.07.2017.
 */
class ChatMessageAdapter(val context: Context, val items: List<ChatMessage>, val listener: (String) -> Unit) : RecyclerView.Adapter<ChatMessageAdapter.ViewHolder>() {

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chat_message, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(chatMessage: ChatMessage, listener: (String) -> Unit) = with(itemView) {
            txtUsername.text = chatMessage.username
            Glide.with(context).load("http://api.adorable.io/avatar/${chatMessage.avatar}").into(imgAvatar)
            imgAvatar.borderColor = chatMessage.color
            txtText.text = chatMessage.text
            txtTime.text = chatMessage.timestamp.getFormattedTime(context)
            //setOnClickListener { listener(chatMessage.text) }
        }
    }
}