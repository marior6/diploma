package cz.marekhaubert.diploma.pubnub

import android.content.Context
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import com.google.gson.JsonObject
import com.pubnub.api.PNConfiguration
import com.pubnub.api.PubNub
import com.pubnub.api.callbacks.PNCallback
import com.pubnub.api.callbacks.SubscribeCallback
import com.pubnub.api.models.consumer.PNPublishResult
import com.pubnub.api.models.consumer.PNStatus
import com.pubnub.api.models.consumer.history.PNHistoryResult
import com.pubnub.api.models.consumer.pubsub.PNMessageResult
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult
import cz.marekhaubert.diploma.ChatMessage
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.base.BaseActivity
import cz.marekhaubert.diploma.toast
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_pubnub.*
import kotlinx.android.synthetic.main.layout_chat_settings.*
import org.joda.time.DateTime
import java.util.*


/**
 * Created by Marek Haubert on 28.07.2017.
 */
class PubnubActivity : BaseActivity() {

    private lateinit var pubnub: PubNub
    private val channelName = "chat"
    private val subscribeKey = "sub-c-db6c8890-7446-11e7-81e6-0619f8945a4f"
    private val publishKey = "pub-c-31566840-5db6-4253-91fc-e446b0808381"
    val list: MutableList<ChatMessage> = mutableListOf()
    lateinit var sharedPreferences: SharedPreferences
    lateinit var avatarViewList: List<CircleImageView>
    lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pubnub)
        setSupportActionBar(toolbar)

        bottomSheetBehavior = BottomSheetBehavior.from(layoutBottomSheet)

        sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE)
        initAvatarViews()

        recyclerPubnub.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        recyclerPubnub.adapter = ChatMessageAdapter(this, list) { s -> toast("Clicked on $s") }
        recyclerPubnub.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        initPubnub()
        updateUserViews()

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                fabSend.visibility = if (slideOffset > 0.5F) View.GONE else View.VISIBLE
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {}
        })

        fabSend.setOnClickListener {
            when {
                editUsername.text.isEmpty() -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    editUsername.error = getString(R.string.mandatory_field)
                }
                editMessage.text.isEmpty() -> editMessage.error = getString(R.string.mandatory_field)
                else -> createChatItem()
            }
        }
        cardColorPicker.setOnClickListener {
            showColorPicker()
        }

        editUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                sharedPreferences.edit().putString("username", text.toString()).apply()
                txtName.text = if (text?.isEmpty()!!) "Anonym" else text.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    /**
     * Inicializuje spojení s databázi PubNub s využitím poskytovatelem přidělených bezpečnostních klíčů.
     */
    private fun initPubnub() {
        val pnConfiguration = PNConfiguration()
        pnConfiguration.subscribeKey = subscribeKey
        pnConfiguration.publishKey = publishKey
        pubnub = PubNub(pnConfiguration)

        // načtení předchozích chat zpráv z PubNub databáze
        loadHistory()
        // nastavení sledování přidání nových příspěvků v databázi
        loadChanges()
    }

    /**
     * Načítá předchozí chat zprávy z PubNub databáze.
     */
    private fun loadHistory() {
        // vyčtení chat zpráv z databáze
        pubnub.history()
                .channel(channelName) // název sledovaného kanálu
                .count(100) // maximální limit načítaných záznamů
                .async(object : PNCallback<PNHistoryResult>() {
                    override fun onResponse(result: PNHistoryResult?, status: PNStatus) {
                        // přidání načtených dat do UI aplikace musí probíhat na hlavním UI vlákně
                        runOnUiThread {
                            // provedení iterace všemi načtenými záznamy z databáze
                            result?.messages?.forEach {
                                val chatMessageJson = it.entry.asJsonObject
                                // deserializace načtených atributů zprávy z formátu JSON na instanci Kotlin třídy
                                val chatMessage = ChatMessage(
                                        username = chatMessageJson.get("username").asString,
                                        avatar = chatMessageJson.get("avatar").asInt,
                                        color = chatMessageJson.get("color").asInt,
                                        text = chatMessageJson.get("text").asString,
                                        timestamp = chatMessageJson.get("timestamp").asLong
                                )
                                // přidání chat zprávy do seznamu
                                list.add(chatMessage)
                            }
                            // chronologické setřídění zpráv podle času jejich vytvoření
                            Collections.sort(list, compareBy { it.timestamp })
                            Collections.reverse(list)
                            // instrukce UI prvku adapter k aktualizaci výpisu chat zpráv
                            recyclerPubnub.adapter.notifyDataSetChanged()
                        }
                    }
                })
    }

    /**
     * Nastavuje sledování přidání nových příspěvků v databázi.
     */
    private fun loadChanges() {
        pubnub.addListener(object : SubscribeCallback() {
            override fun status(pubnub: PubNub?, status: PNStatus?) {
                println(status)
            }

            override fun presence(pubnub: PubNub?, presence: PNPresenceEventResult?) {
                println(presence)
            }

            /**
             * posluchač nově přidaných zpráv do chat databáze
             */
            override fun message(pubnub: PubNub, message: PNMessageResult) {
                runOnUiThread {
                    val messageJson = message.message.asJsonObject
                    // deserializace načtených atributů zprávy z formátu JSON na instanci Kotlin třídy
                    val chatMessage = ChatMessage(
                            username = messageJson.get("username").asString,
                            avatar = messageJson.get("avatar").asInt,
                            color = messageJson.get("color").asInt,
                            text = messageJson.get("text").asString,
                            timestamp = messageJson.get("timestamp").asLong
                    )
                    // přidání chat zprávy do seznamu
                    list.add(0, chatMessage)
                    // instrukce UI prvku adapter k zobrazení nového záznamu ve výpisu chat zpráv
                    recyclerPubnub.adapter.notifyItemInserted(0)
                    // instrukce UI prvku adapter k posunutí zobrazení na nejnovější záznam
                    recyclerPubnub.scrollToPosition(0)
                }
            }
        })
        // nastavení odposlechu zpráv v kanálu @channelName
        pubnub.subscribe().channels(Arrays.asList(channelName)).execute()
    }

    /**
     * Vytváří nový záznam a odesílá ho k uložení do PubNub databáze.
     */
    private fun createChatItem() {
        // vytvoření objektu a naplnění daty
        val message = JsonObject()
        message.addProperty("username", sharedPreferences.getString("username", "Anonym"))
        message.addProperty("avatar", sharedPreferences.getInt("avatar", 0))
        message.addProperty("color", sharedPreferences.getInt("color", Color.BLACK))
        message.addProperty("timestamp", DateTime().millis / 1000L)
        message.addProperty("text", editMessage.text.toString())

        // publikování nového chat záznamu do PubNub kanálu @channelName
        pubnub.publish()
                .message(message)
                .channel(channelName)
                .async(object : PNCallback<PNPublishResult>() {
                    override fun onResponse(result: PNPublishResult, status: PNStatus) {
                        Log.d("CREATE CHAT MESSAGE", result.toString())
                    }
                })

        // úprava stavu UI prvků po odeslání zprávy
        editMessage.setText("")
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) handleSettingsVisibility()
        else super.onBackPressed()
    }

    private fun initAvatarViews() {
        avatarViewList = listOf(imgAvatarSelect1, imgAvatarSelect2, imgAvatarSelect3, imgAvatarSelect4, imgAvatarSelect5, imgAvatarSelect6, imgAvatarSelect7)
        avatarViewList.forEach { it.borderColor = Color.WHITE }
        avatarViewList[sharedPreferences.getInt("avatar", 0)].borderColor = Color.BLACK
        avatarViewList.forEach {
            Glide.with(this@PubnubActivity).load("http://api.adorable.io/avatar/${avatarViewList.indexOf(it)}").into(it)
            it.setOnClickListener({
                sharedPreferences.edit().putInt("avatar", avatarViewList.indexOf(it)).apply()
                avatarViewList.forEach { it.borderColor = Color.WHITE }
                (it as CircleImageView).borderColor = Color.BLACK
                updateUserProfileView()
            })
        }
        layoutUserInfo.setOnClickListener({ handleSettingsVisibility() })
        updateUserProfileView()
    }

    private fun updateUserProfileView() {
        Glide.with(this@PubnubActivity).load("http://api.adorable.io/avatar/${sharedPreferences.getInt("avatar", 0)}").into(imgAvatar)
        imgAvatar.borderColor = sharedPreferences.getInt("color", Color.BLACK)
    }

    private fun handleSettingsVisibility() {
        bottomSheetBehavior.state = if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
            BottomSheetBehavior.STATE_COLLAPSED else BottomSheetBehavior.STATE_EXPANDED
    }

    private fun showColorPicker() {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle(R.string.choose_color)
                .initialColor(sharedPreferences.getInt("color", Color.BLACK))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(R.string.dialog_ok) { dialog, selectedColor, allColors ->
                    sharedPreferences.edit().putInt("color", selectedColor).apply()
                    updateUserViews()
                }
                .setNegativeButton(R.string.dialog_cancel, { dialog, which -> dialog.dismiss() })
                .build()
                .show()
    }

    private fun updateUserViews() {
        editUsername.setText(sharedPreferences.getString("username", "Anonym"))
        txtName.text = sharedPreferences.getString("username", "Anonym")
        cardColorPicker.cardBackgroundColor = ColorStateList.valueOf(sharedPreferences.getInt("color", Color.BLACK))
        imgAvatar.borderColor = sharedPreferences.getInt("color", Color.BLACK)
    }

    override fun onDestroy() {
        pubnub.unsubscribe().channels(Arrays.asList(channelName)).execute()
        super.onDestroy()
    }
}
