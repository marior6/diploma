package cz.marekhaubert.diploma

import android.app.Application
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.facebook.stetho.Stetho
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import io.cloudboost.CloudApp
import io.reactivex.plugins.RxJavaPlugins
import io.realm.*
import timber.log.Timber

/**
 * Created by Marek Haubert on 28.07.2017.
 */
class App : Application() {

    private val TAG = App::class.java.name
    private val SCHEMA_VERSION = 2L

    override fun onCreate() {
        super.onCreate()
        initRealm()
        initCloudBoost()

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build())
        Timber.plant(Timber.DebugTree())
        RxJavaPlugins.setErrorHandler({ Timber.e(it) })
    }

    private fun initCloudBoost() {
        val appId = "guelfrkejkzs"
        val appKey = "c29287c6-c3df-4b62-9349-e95b99230509"
        CloudApp.init(appId, appKey)
    }

    private fun initRealm() {

        Realm.init(this)

        logoutExistingUser()
        val syncCredentials = SyncCredentials.usernamePassword("haubert.marek@gmail.com", "subzero", false)

        SyncUser.loginAsync(syncCredentials, "http://81.0.209.5:9080", object : SyncUser.Callback {
            override fun onSuccess(user: SyncUser) {
                user.let { setRealmDefaultConfig(it) }
            }

            override fun onError(error: ObjectServerError) {
                Log.e("Init Realm", "Error connecting", error)

                when (error.errorCode) {
                    ErrorCode.INVALID_CREDENTIALS -> toast("Error: Invalid Credentials...")
                    else -> toast("Error: Could not connect, check host...")
                }
            }
        })
    }

    private fun setRealmDefaultConfig(user: SyncUser) {
        Log.d(TAG, "Connecting to Sync Server at : [" + "realm://81.0.209.5:9080/~/diploma".replace("~".toRegex(), user.identity) + "]")
        Realm.removeDefaultConfiguration()
        val syncConfiguration = SyncConfiguration.Builder(user, "realm://81.0.209.5:9080/~/diploma").schemaVersion(SCHEMA_VERSION).build()
        Realm.setDefaultConfiguration(syncConfiguration)
    }

    private fun logoutExistingUser() {
        val user = SyncUser.currentUser()
        user?.logout()
    }
}
