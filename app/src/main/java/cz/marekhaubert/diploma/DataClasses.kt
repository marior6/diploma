package cz.marekhaubert.diploma

import com.google.gson.Gson
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/**
 * Created by Marek Haubert on 29.07.2017.
 */
data class ChatMessage(var username: String, var avatar: Int, var color: Int, var timestamp: Long, var text: String)

interface IUserItem {
    var id:Long
    var name:String
    var email:String
    var phone:String
    var imageId:Int
}

@RealmClass
open class UserRealm(@PrimaryKey override var id: Long, override var name: String, override var email: String, override var phone: String, override var imageId: Int) : RealmObject(), IUserItem {
    constructor() : this(0, "", "", "", 1)
}

data class User(override var id: Long, override var name: String, override var email: String, override var phone: String, override var imageId: Int, var actionId: Int) : IUserItem {
    constructor() : this(0, "", "", "",0, 1)
}

@RealmClass
open class RealmHolder(@PrimaryKey open var id: String, var value: Int) : RealmObject() {
    constructor() : this("primary", 0)
}

data class UserDS(var id: String, var email: String, var online: Boolean, var score: Int)
data class MessageDS(var id: String, var email: String, var content: String, var msgId: String)
data class ButtonItem(var id: Int, var width: Int, var height: Int, var active: Boolean)

open class StateRegistry internal constructor() {
    internal var userId: String? = null
    var email: String? = null
    var gson: Gson? = null

    companion object {
        val instance = StateRegistry()
    }
}