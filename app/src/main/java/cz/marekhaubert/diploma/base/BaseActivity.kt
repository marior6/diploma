package cz.marekhaubert.diploma.base

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import cz.marekhaubert.diploma.*
import cz.marekhaubert.diploma.cloudboost.CloudBoostActivity
import cz.marekhaubert.diploma.deepstream.DeepStreamActivity
import cz.marekhaubert.diploma.deepstream.DeepStreamLoginActivity
import cz.marekhaubert.diploma.firebase.FirebaseActivity
import cz.marekhaubert.diploma.pubnub.PubnubActivity
import cz.marekhaubert.diploma.realm.ui.RealmActivity
import cz.marekhaubert.diploma.test.RealmCompareActivity
import cz.marekhaubert.diploma.test.WebsocketCompareActivity
import org.jetbrains.anko.longToast
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder




/**
 * Created by Marek Haubert on 28.07.2017.
 */
abstract class BaseActivity : AppCompatActivity() {

    private val CAMERA_REQUEST_CODE: Int = 24

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_option_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_firebase -> {
                startActivity(Intent(this, FirebaseActivity::class.java))
                finish()
            }
            R.id.nav_pubnub -> {
                startActivity(Intent(this, PubnubActivity::class.java))
                finish()
            }
            R.id.nav_realm -> {
                createRealmActivity()
            }
            R.id.nav_deepstream -> {
                startActivity(Intent(this, if (StateRegistry.instance.userId == null) DeepStreamLoginActivity::class.java else DeepStreamActivity::class.java))
                finish()
            }
            R.id.nav_cloudboost -> {
                startActivity(Intent(this, CloudBoostActivity::class.java))
                finish()
            }
            R.id.nav_compare_realm -> {
                startActivity(Intent(this, RealmCompareActivity::class.java))
                finish()
            }
            R.id.nav_compare_websockets -> {
                startActivity(Intent(this, WebsocketCompareActivity::class.java))
                finish()
            }
            R.id.libraries -> {
                LibsBuilder()
                        .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                        .withActivityTitle(getString(R.string.libraries))
                        .start(this)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun createRealmActivity() {
        if (ContextCompat.checkSelfPermission(this@BaseActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@BaseActivity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), CAMERA_REQUEST_CODE)
        } else {
            startActivity(Intent(this, RealmActivity::class.java))
            finish()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createRealmActivity()
            } else {
                longToast(getString(R.string.error_permission_needed))
            }
        }
    }
}
