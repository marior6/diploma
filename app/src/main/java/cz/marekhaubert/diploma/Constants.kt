package cz.marekhaubert.diploma

/**
 * Created by Marek Haubert on 20.08.2017.
 */
val NEWS_KEY = "news_key"
val NEWS_TITLE = "news_title"

val WEBSOCKET_SERVER_URL = "ws://89.221.210.58:8080/diplomaApi/diploma/websocket"
val REST_SERVER_URL = "http://89.221.210.58:8080/diplomaApi/"