package cz.marekhaubert.diploma.realm.ui

import android.app.AlertDialog
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.Switch
import com.vuforia.*
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.RealmHolder
import cz.marekhaubert.diploma.base.BaseActivity
import cz.marekhaubert.diploma.realm.config.SampleApplicationControl
import cz.marekhaubert.diploma.realm.config.SampleApplicationException
import cz.marekhaubert.diploma.realm.config.SampleApplicationSession
import cz.marekhaubert.diploma.realm.utils.LoadingDialogHandler
import cz.marekhaubert.diploma.realm.utils.SampleApplicationGLView
import cz.marekhaubert.diploma.realm.utils.Texture
import io.realm.*
import kotlinx.android.synthetic.main.activity_realm.*
import java.util.*

/**
 * Created by Vuforia on 28.08.2017.
 */
class RealmActivity : BaseActivity(), SampleApplicationControl {

    val realm: Realm = Realm.getDefaultInstance()

    private lateinit var vuforiaAppSession: SampleApplicationSession

    private var mCurrentDataset: DataSet? = null
    private val mCurrentDatasetSelectionIndex = 0
    private val mDatasetStrings = ArrayList<String>()

    // Our OpenGL view:
    private var mGlView: SampleApplicationGLView? = null

    // Our renderer:
    private var renderer: RealmRenderer? = null

    private var mGestureDetector: GestureDetector? = null

    // The textures we will use for rendering:
    private var mTextures: Vector<Texture>? = null

    private var mSwitchDatasetAsap = false
    private val mFlash = false
    private var mContAutofocus = false

    private val mFlashOptionView: View? = null

    private var mUILayout: FrameLayout? = null

    var loadingDialogHandler = LoadingDialogHandler(this)

    // Alert Dialog used to display SDK errors
    private var mErrorDialog: AlertDialog? = null

    private var mIsDroidDevice = false

    private var realmHolder: RealmHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(LOGTAG, "onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realm)
        setSupportActionBar(toolbar)
        setButtonsVisibility(false)

        vuforiaAppSession = SampleApplicationSession(this)

        startLoadingAnimation()
        mDatasetStrings.add("StonesAndChips.xml")
        mDatasetStrings.add("Tarmac.xml")

        vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

        mGestureDetector = GestureDetector(this, GestureListener())

        // Load any sample specific textures:
        mTextures = Vector()
        loadTextures()

        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid")
    }

    /**
     * Provádí inicializaci záznamu z Realm databáze.
     */
    private fun initRealmValues() {
        try {
            // vyčtení záznamu z Realm databáze, není-li záznam v databázi nalezen, je vytvořena vyjímka
            realmHolder = realm.where(RealmHolder::class.java).equalTo("id", "primary").findFirst()
        } catch (exception: Exception) {
            exception.printStackTrace()
            // pokud není v databázi nalezen požadovaný záznam, je vytvořená výjimka zde odchycena a do databáze je uložen požadovaný záznam s defaultními parametry
            realm.executeTransaction { realmLocal -> realmHolder = realmLocal?.copyToRealmOrUpdate(RealmHolder()) }
        }
        // vyčtený záznam z databáze je opatřen posluchačem událostí, který při každé zaznamenané změně uloženého objektu provede změnu UI
        realmHolder?.addChangeListener(RealmObjectChangeListener<RealmHolder> { newRealmHolder, _ ->
            // při změně údajů objektu v databázi je provedena změna textury krychle v UI
            renderer?.textureIndex = newRealmHolder.value
        })
        // prvotní nastavení textury krychle
        setTexture(realmHolder?.value!!)

        // data z databáze jsou načtena a v UI aplikace tak mohou být zobrazena tlačítka pro změnu textury krychle
        setButtonsVisibility(true)

        // tlačítkům pro změnu textury krychle je nastaven posluchač události kliknutí, který uloží do databáze novou hodnotu textury krychle
        btnTexture1.setOnClickListener({ setTexture(0) })
        btnTexture2.setOnClickListener({ setTexture(1) })
        btnTexture3.setOnClickListener({ setTexture(2) })
    }

    /**
     * Nastavuje viditelnost tlačítek pro změnu textury krychle.
     */
    private fun setButtonsVisibility(visible: Boolean) {
        btnTexture1.visibility = if (visible) View.VISIBLE else View.GONE
        btnTexture2.visibility = if (visible) View.VISIBLE else View.GONE
        btnTexture3.visibility = if (visible) View.VISIBLE else View.GONE
    }

    /**
     * Ukládá do databáze novou hodnotu textury krychle, změna se pak projeví v posluchači událostí změny uloženého databázového objektu v metodě initRealmValues().
     */
    private fun setTexture(textureId: Int) {
        realm.executeTransaction({
            realmHolder?.value = textureId
        })
    }

    // Process Single Tap event to trigger autofocus
    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        // Used to set autofocus one second after a manual focus is triggered
        private val autofocusHandler = Handler()


        override fun onDown(e: MotionEvent): Boolean = true


        override fun onSingleTapUp(e: MotionEvent): Boolean {
            autofocusHandler.postDelayed({
                val result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)
                if (!result)
                    Log.e("SingleTapUp", "Unable to trigger focus")
            }, 1000L)

            return true
        }
    }

    private fun loadTextures() {
        mTextures!!.add(Texture.loadTextureFromApk("vse_logo_blue.png", assets))
        mTextures!!.add(Texture.loadTextureFromApk("vse_logo_purple.png", assets))
        mTextures!!.add(Texture.loadTextureFromApk("vse_logo_red.png", assets))
    }

    override fun onResume() {
        Log.d(LOGTAG, "onResume")
        super.onResume()
        if (mIsDroidDevice) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        try {
            vuforiaAppSession.resumeAR()
        } catch (e: SampleApplicationException) {
            Log.e(LOGTAG, e.string)
        }

        if (mGlView != null) {
            mGlView!!.visibility = View.VISIBLE
            mGlView!!.onResume()
        }

    }

    override fun onConfigurationChanged(config: Configuration) {
        Log.d(LOGTAG, "onConfigurationChanged")
        super.onConfigurationChanged(config)

        vuforiaAppSession.onConfigurationChanged()
    }

    override fun onPause() {
        Log.d(LOGTAG, "onPause")
        super.onPause()

        if (mGlView != null) {
            mGlView!!.visibility = View.INVISIBLE
            mGlView!!.onPause()
        }

        // Turn off the flash
        if (mFlashOptionView != null && mFlash) {
            (mFlashOptionView as Switch).isChecked = false
        }

        try {
            vuforiaAppSession.pauseAR()
        } catch (e: SampleApplicationException) {
            Log.e(LOGTAG, e.string)
        }

    }

    override fun onStop() {
        super.onStop()
        if (realm != null) {
            realm.removeAllChangeListeners()
            if (realm.isInTransaction) {
                realm.cancelTransaction()
            }
            realm.close()
        }
    }

    override fun onDestroy() {
        Log.d(LOGTAG, "onDestroy")
        super.onDestroy()

        try {
            vuforiaAppSession.stopAR()
        } catch (e: SampleApplicationException) {
            Log.e(LOGTAG, e.string)
        }

        // Unload texture:
        mTextures!!.clear()
        mTextures = null

        System.gc()
    }


    private fun initApplicationAR() {
        // Create OpenGL ES view:
        val depthSize = 16
        val stencilSize = 0
        val translucent = Vuforia.requiresAlpha()

        mGlView = SampleApplicationGLView(this)
        mGlView!!.init(translucent, depthSize, stencilSize)

        renderer = RealmRenderer(this, Realm.getDefaultInstance(), vuforiaAppSession)
        renderer!!.setTextures(mTextures)
        mGlView!!.setRenderer(renderer)

        initRealmValues()
    }


    private fun startLoadingAnimation() {
        mUILayout = layoutCameraOverlay
        mUILayout!!.visibility = View.VISIBLE
        mUILayout!!.setBackgroundColor(Color.BLACK)
        loadingDialogHandler.mLoadingDialogContainer = loadingIndicator
        loadingDialogHandler.sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG)

    }

    override fun doLoadTrackersData(): Boolean {
        val tManager = TrackerManager.getInstance()
        val objectTracker = tManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet()

        if (mCurrentDataset == null)
            return false

        if (!mCurrentDataset!!.load(
                mDatasetStrings[mCurrentDatasetSelectionIndex],
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false

        val numTrackables = mCurrentDataset!!.numTrackables
        for (count in 0 until numTrackables) {
            val trackable = mCurrentDataset!!.getTrackable(count)

            val name = "Current Dataset : " + trackable.name
            trackable.userData = name
            Log.d(LOGTAG, "UserData:Set the following user data " + trackable.userData)
        }
        return true
    }


    override fun doUnloadTrackersData(): Boolean {
        var result = true

        val tManager = TrackerManager.getInstance()
        val objectTracker = tManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker

        if (mCurrentDataset != null && mCurrentDataset!!.isActive) {
            if (objectTracker.getActiveDataSet(0) == mCurrentDataset && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                result = false
            } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                result = false
            }

            mCurrentDataset = null
        }

        return result
    }


    override fun onInitARDone(exception: SampleApplicationException?) {
        if (exception == null) {
            initApplicationAR()

            renderer!!.setActive(true)
            layoutCameraOverlay.removeAllViews()
            layoutCameraOverlay.addView(mGlView)
            mUILayout!!.setBackgroundColor(Color.TRANSPARENT)

            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT)
            } catch (e: SampleApplicationException) {
                Log.e(LOGTAG, e.string)
            }

            val result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO)
            if (result) mContAutofocus = true else Log.e(LOGTAG, "Unable to enable continuous autofocus")

        } else {
            Log.e(LOGTAG, exception.string)
            showInitializationErrorMessage(exception.string)
        }
    }

    private fun showInitializationErrorMessage(message: String) {
        runOnUiThread {
            if (mErrorDialog != null) {
                mErrorDialog!!.dismiss()
            }
            val builder = AlertDialog.Builder(
                    this@RealmActivity)
            builder
                    .setMessage(message)
                    .setTitle(getString(R.string.INIT_ERROR))
                    .setCancelable(false)
                    .setIcon(0)
                    .setPositiveButton(getString(R.string.button_OK)
                    ) { _, _ -> finish() }
            mErrorDialog = builder.create()
            mErrorDialog!!.show()
        }
    }


    override fun onVuforiaUpdate(state: State) {
        if (mSwitchDatasetAsap) {
            mSwitchDatasetAsap = false
            val tm = TrackerManager.getInstance()
            val ot = tm.getTracker(ObjectTracker
                    .getClassType()) as ObjectTracker
            if (mCurrentDataset == null || ot.getActiveDataSet(0) == null) {
                Log.d(LOGTAG, "Failed to swap datasets")
                return
            }
            doUnloadTrackersData()
            doLoadTrackersData()
        }
    }


    override fun doInitTrackers(): Boolean {
        var result = true
        val tManager = TrackerManager.getInstance()
        val tracker: Tracker?
        tracker = tManager.initTracker(ObjectTracker.getClassType())
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started")
            result = false
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized")
        }
        return result
    }

    override fun doStartTrackers(): Boolean {
        val result = true
        val objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType())
        objectTracker?.start()
        return result
    }


    override fun doStopTrackers(): Boolean {
        val result = true
        val objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType())
        objectTracker?.stop()

        return result
    }


    override fun doDeinitTrackers(): Boolean {
        val result = true
        val tManager = TrackerManager.getInstance()
        tManager.deinitTracker(ObjectTracker.getClassType())
        return result
    }

    override fun onTouchEvent(event: MotionEvent): Boolean = mGestureDetector!!.onTouchEvent(event)

    companion object {
        private val LOGTAG = "ImageTargets"
    }
}