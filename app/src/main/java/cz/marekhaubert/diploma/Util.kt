package cz.marekhaubert.diploma

import com.google.firebase.database.FirebaseDatabase
import cz.marekhaubert.diploma.firebase.Author
import cz.marekhaubert.diploma.firebase.Category
import cz.marekhaubert.diploma.firebase.News
import io.deepstream.DeepstreamClient
import org.joda.time.DateTime
import java.util.*

/**
 * Created by Marek on 26.10.2017.
 */

fun populateFirebaseDatabase(database: FirebaseDatabase?) {

    database?.getReference("category")?.child("category0")?.setValue(Category("Domácí", "#f44336"))
    database?.getReference("category")?.child("category1")?.setValue(Category("Zahraniční", "#9c27b0"))
    database?.getReference("category")?.child("category2")?.setValue(Category("Společnost", "#3f51b5"))
    database?.getReference("category")?.child("category3")?.setValue(Category("Kultura", "#2196f3"))
    database?.getReference("category")?.child("category4")?.setValue(Category("Ekonomika", "#4caf50"))
    database?.getReference("category")?.child("category5")?.setValue(Category("Věda", "#cddc39"))
    database?.getReference("category")?.child("category6")?.setValue(Category("Cestování", "#ff5722"))
    database?.getReference("category")?.child("category7")?.setValue(Category("Sport", "#607d8b"))

    val authorKey1 = database?.getReference("author")?.push()?.key
    val authorKey2 = database?.getReference("author")?.push()?.key
    val authorKey3 = database?.getReference("author")?.push()?.key

    database?.getReference("author")?.child(authorKey1)?.setValue(Author("John Doe", "http://api.adorable.io/avatar/johndoe"))
    database?.getReference("author")?.child(authorKey2)?.setValue(Author("Steve Jackson", "http://api.adorable.io/avatar/stevejackson"))
    database?.getReference("author")?.child(authorKey3)?.setValue(Author("Peter Miller", "http://api.adorable.io/avatar/petermiller"))

    val newsKey1 = database?.getReference("news")?.push()?.key
    val newsKey2 = database?.getReference("news")?.push()?.key
    val newsKey3 = database?.getReference("news")?.push()?.key
    val newsKey4 = database?.getReference("news")?.push()?.key
    val newsKey5 = database?.getReference("news")?.push()?.key
    val newsKey6 = database?.getReference("news")?.push()?.key
    val newsKey7 = database?.getReference("news")?.push()?.key
    val newsKey8 = database?.getReference("news")?.push()?.key
    val newsKey9 = database?.getReference("news")?.push()?.key
    val newsKey10 = database?.getReference("news")?.push()?.key
    val newsKey11 = database?.getReference("news")?.push()?.key
    val newsKey12 = database?.getReference("news")?.push()?.key
    val newsKey13 = database?.getReference("news")?.push()?.key
    val newsKey14 = database?.getReference("news")?.push()?.key
    val newsKey15 = database?.getReference("news")?.push()?.key
    val newsKey16 = database?.getReference("news")?.push()?.key
    val newsKey17 = database?.getReference("news")?.push()?.key
    val newsKey18 = database?.getReference("news")?.push()?.key
    val newsKey19 = database?.getReference("news")?.push()?.key
    val newsKey20 = database?.getReference("news")?.push()?.key
    val newsKey21 = database?.getReference("news")?.push()?.key
    val newsKey22 = database?.getReference("news")?.push()?.key
    val newsKey23 = database?.getReference("news")?.push()?.key
    val newsKey24 = database?.getReference("news")?.push()?.key

    val numberRandom = Random(100)

    database?.getReference("news")?.child(newsKey1)?.setValue(
            News("",
                    "Phasellus ut aliquam",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nature/${numberRandom.nextInt(10)}",
                    "category0"))

    database?.getReference("news")?.child(newsKey2)?.setValue(
            News("",
                    "Nulla elementum tortor quam",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nature/${numberRandom.nextInt(10)}",
                    "category0"))

    database?.getReference("news")?.child(newsKey3)?.setValue(
            News("",
                    "Integer mauris justo",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nature/${numberRandom.nextInt(10)}",
                    "category0"))

    database?.getReference("news")?.child(newsKey4)?.setValue(
            News("",
                    "Donec id massa",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nightlife/${numberRandom.nextInt(10)}",
                    "category1"))

    database?.getReference("news")?.child(newsKey5)?.setValue(
            News("",
                    "Etiam interdum placerat rhoncus",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nightlife/${numberRandom.nextInt(10)}",
                    "category1"))

    database?.getReference("news")?.child(newsKey6)?.setValue(
            News("",
                    "Nulla arcu leo",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/nightlife/${numberRandom.nextInt(10)}",
                    "category1"))

    database?.getReference("news")?.child(newsKey7)?.setValue(
            News("",
                    "Morbi congue vulputate",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/people/${numberRandom.nextInt(10)}",
                    "category2"))

    database?.getReference("news")?.child(newsKey8)?.setValue(
            News("",
                    "Nunc ullamcorper ex",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/people/${numberRandom.nextInt(10)}",
                    "category2"))

    database?.getReference("news")?.child(newsKey9)?.setValue(
            News("",
                    "Praesent efficitur rutrum",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/people/${numberRandom.nextInt(10)}",
                    "category2"))

    database?.getReference("news")?.child(newsKey10)?.setValue(
            News("",
                    "Etiam sagittis a ipsum",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/abstract/${numberRandom.nextInt(10)}",
                    "category3"))

    database?.getReference("news")?.child(newsKey11)?.setValue(
            News("",
                    "Lorem ipsum dolor",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/abstract/${numberRandom.nextInt(10)}",
                    "category3"))

    database?.getReference("news")?.child(newsKey12)?.setValue(
            News("",
                    "Donec pulvinar pellentesque",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/abstract/${numberRandom.nextInt(10)}",
                    "category3"))

    database?.getReference("news")?.child(newsKey13)?.setValue(
            News("",
                    "Ut dictum",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/business/${numberRandom.nextInt(10)}",
                    "category4"))

    database?.getReference("news")?.child(newsKey14)?.setValue(
            News("",
                    "Aenean mi leo, facilisis a ornare nec",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/business/${numberRandom.nextInt(10)}",
                    "category4"))

    database?.getReference("news")?.child(newsKey15)?.setValue(
            News("",
                    "In pretium nisl",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/business/${numberRandom.nextInt(10)}",
                    "category4"))

    database?.getReference("news")?.child(newsKey16)?.setValue(
            News("",
                    "Nulla vitae tellus",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/technics/${numberRandom.nextInt(10)}",
                    "category5"))

    database?.getReference("news")?.child(newsKey17)?.setValue(
            News("",
                    "Sed ac ullamcorper tellus",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/technics/${numberRandom.nextInt(10)}",
                    "category5"))

    database?.getReference("news")?.child(newsKey18)?.setValue(
            News("",
                    "Curabitur cursus enim in elit",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/technics/${numberRandom.nextInt(10)}",
                    "category5"))

    database?.getReference("news")?.child(newsKey19)?.setValue(
            News("",
                    "Nullam vitae",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/city/${numberRandom.nextInt(10)}",
                    "category6"))

    database?.getReference("news")?.child(newsKey20)?.setValue(
            News("",
                    "Orci varius natoque penatibus et",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/city/${numberRandom.nextInt(10)}",
                    "category6"))

    database?.getReference("news")?.child(newsKey21)?.setValue(
            News("",
                    "Quisque ac augue",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/city/${numberRandom.nextInt(10)}",
                    "category6"))

    database?.getReference("news")?.child(newsKey22)?.setValue(
            News("",
                    "Donec in turpis euismod, venenatis metus in",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey1!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/sports/${numberRandom.nextInt(10)}",
                    "category7"))

    database?.getReference("news")?.child(newsKey23)?.setValue(
            News("",
                    "Duis sit amet mi sed",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey2!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/sports/${numberRandom.nextInt(10)}",
                    "category7"))

    database?.getReference("news")?.child(newsKey24)?.setValue(
            News("",
                    "Nunc eu libero a odio",
                    "Quisque nibh nulla, maximus eget tortor eget, vulputate dignissim tortor. Mauris euismod turpis ut purus gravida molestie. Vivamus volutpat tincidunt neque, non ultricies dui vestibulum ullamcorper.",
                    "Phasellus viverra dui feugiat massa fermentum, sit amet feugiat erat consequat. Ut aliquet felis nec iaculis mollis. Nullam laoreet dolor id magna volutpat, vel pretium lorem accumsan. Morbi nec diam suscipit, mollis velit sed, blandit nisl. Aliquam nec neque tristique, accumsan felis a, pellentesque justo. Mauris lacinia ornare est, eu semper nulla sagittis in.\n\nMaecenas ligula magna, tempus sit amet velit iaculis, faucibus consequat nulla. Pellentesque congue, odio in volutpat aliquam, augue ante consequat dui, ac accumsan lorem massa in eros. Quisque nec tristique nibh. In placerat eros a ante vestibulum, ac lobortis mi accumsan. Mauris sollicitudin congue erat, sed mollis nisi mollis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam eget dolor vitae nisi porta ornare ut at dolor. Aenean vitae fermentum erat, sed dapibus enim. Phasellus semper nulla ex, gravida tincidunt enim eleifend et. Fusce pharetra diam erat, id lacinia odio finibus viverra. Pellentesque consequat lacus sit amet malesuada varius. Phasellus id porttitor magna. Praesent ac faucibus velit, eget malesuada nisi.\n\nProin vel mauris egestas, aliquam sem eget, tincidunt diam. Morbi quis vulputate orci. Integer iaculis hendrerit blandit. Suspendisse hendrerit placerat augue, rutrum vulputate ex aliquam id. Aliquam at vestibulum dui, id malesuada justo. ",
                    authorKey3!!, null, DateTime().millis / 1000,
                    "http://lorempixel.com/400/200/sports/${numberRandom.nextInt(10)}",
                    "category7"))
}

fun populateDeepStreamDatabase(client: DeepstreamClient?, stateRegistry: StateRegistry) {
    listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).forEach {
        val createButton = client!!.record.getRecord("Button/$it")
        val buttonItem = ButtonItem(it, it * 10, it * 10, it == 9)
        createButton.set(stateRegistry.gson?.toJsonTree(buttonItem))
    }
}