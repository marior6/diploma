package cz.marekhaubert.diploma

import android.content.Context
import android.support.v4.app.Fragment
import android.widget.Toast
import org.jetbrains.anko.bundleOf
import org.joda.time.DateTime
import org.joda.time.Hours
import org.joda.time.Minutes
import org.joda.time.format.DateTimeFormat
import java.util.*

/**
 * Created by Marek Haubert on 29.07.2017.
 */
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

inline fun <reified T : Fragment> instanceOf(vararg params: Pair<String, Any>)
        = T::class.java.newInstance().apply {
    arguments = bundleOf(*params)
}

fun Long.getFormattedTime(context: Context): String {
    val publishedDateTime = DateTime(this * 1000L)
    val currentDateTime = DateTime()

    return when {
        currentDateTime.minusHours(24).isAfter(publishedDateTime) -> {
            val outputFormatter = DateTimeFormat.forPattern("d.M.YYYY HH:mma")
            outputFormatter.withLocale(Locale.US)
            publishedDateTime.toString(outputFormatter)
        }
        currentDateTime.minusHours(1).isAfter(publishedDateTime) -> {
            val hours = Hours.hoursBetween(publishedDateTime, currentDateTime).hours
            context.getString(R.string.date_hours_placeholder, hours)
        }
        else -> {
            val minutes = Minutes.minutesBetween(publishedDateTime, currentDateTime).minutes
            if (minutes == 0) context.getString(R.string.date_now) else context.getString(R.string.date_minutes_placeholder, minutes)
        }
    }
}

fun Long.getShortFormattedTime(): String {
    val publishedDateTime = DateTime(this)
    val outputFormatter = DateTimeFormat.forPattern("d.M.")
    outputFormatter.withLocale(Locale.US)
    return publishedDateTime.toString(outputFormatter)
}