package cz.marekhaubert.diploma.cloudboost

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.base.BaseActivity
import cz.marekhaubert.diploma.getShortFormattedTime
import io.cloudboost.CloudObjectArrayCallback
import io.cloudboost.CloudQuery
import kotlinx.android.synthetic.main.activity_cloudboost.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Marek Haubert on 30.07.2017.
 */
class CloudBoostActivity : BaseActivity() {

    private val temperatureMap: MutableMap<Float, Pair<Long, Double>> = mutableMapOf()
    private val atomicInt = AtomicInteger(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cloudboost)
        setSupportActionBar(toolbar)
        atomicInt.set(0)
        doAsync { loadData() }
    }

    /**
     * Načítá teplotní data z CloudBoost databáze.
     */
    private fun loadData() {
        // vytvoření nového dotazu do databázové tabulky s názvem Temperature
        val query = CloudQuery("Temperature")
        // nastavení limitu na maximum 100 záznamů, seřazených sestupně dle data vytvoření, aby byly zobrazeny pouze nejnovější měření
        query.setLimit(100).orderByDesc("createdAt").find(CloudObjectArrayCallback { cloudObjectArray, cloudException ->
            runOnUiThread {
                // provedení iterace veškerých načtených záznamů z databáze
                cloudObjectArray?.forEach { cloudObject ->
                    val inputFormatter = ISODateTimeFormat.dateTime()
                    val publishedDateTime = inputFormatter.parseDateTime(cloudObject.getString("createdAt"))
                    val value = try {
                        cloudObject.getDouble("value")
                    } catch (e: Exception) {
                        cloudObject.getInteger("value").toDouble()
                    }
                    // přidání záznamu měření do datové struktury k zobrazení v UI
                    temperatureMap.put(atomicInt.getAndIncrement().toFloat(), Pair(publishedDateTime.millis, value))
                }
                // zobrazení načtených dat v grafu
                showDataInChart(temperatureMap)
                // v případě výskytu chyby během načítání dat je uživateli zobrazena chybová hláška
                cloudException?.message?.let { toast(it) }
            }
        })
    }

    private fun showDataInChart(temperatureMap: MutableMap<Float, Pair<Long, Double>>) {
        val entries = mutableListOf<Entry>()

        temperatureMap.iterator().forEach { entry -> entries.add(Entry(entry.key, entry.value.second.toFloat())) }
        val dataSet = LineDataSet(entries, "Temperature")
        dataSet.color = ContextCompat.getColor(this, R.color.colorPrimary)
        dataSet.valueTextColor = ContextCompat.getColor(this, R.color.colorAccent)

        val temperatureSortedMap = temperatureMap.toSortedMap()
        val firstDateValue = temperatureSortedMap[temperatureSortedMap.firstKey()]?.first
        lineChartTemperature.xAxis.valueFormatter = XAxisValueFormatter(this, firstDateValue)
        lineChartTemperature.data = LineData(dataSet)
        lineChartTemperature.invalidate()
    }

    class XAxisValueFormatter(val context: Context, val firstDateValue: Long?) : IAxisValueFormatter {
        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            val actualDateTime = DateTime(firstDateValue).minusHours(value.toInt()).millis
            return actualDateTime.getShortFormattedTime()
        }
    }
}
