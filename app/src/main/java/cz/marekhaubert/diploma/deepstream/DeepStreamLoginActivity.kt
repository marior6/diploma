package cz.marekhaubert.diploma.deepstream

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.JsonObject
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.StateRegistry
import cz.marekhaubert.diploma.UserDS
import cz.marekhaubert.diploma.base.BaseActivity
import io.deepstream.DeepstreamClient
import io.deepstream.DeepstreamFactory
import io.deepstream.LoginResult
import io.deepstream.MergeStrategy
import kotlinx.android.synthetic.main.activity_deepstream_login.*
import java.io.BufferedWriter
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URISyntaxException
import java.net.URL
import java.util.*


/**
 * Created by DeepStream.
 */
class DeepStreamLoginActivity : BaseActivity() {

    private var authTask: UserLoginTask? = null

    private var deepstreamFactory: DeepstreamFactory? = null
    private var client: DeepstreamClient? = null
    private var stateRegistry: StateRegistry? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deepstream_login)
        setSupportActionBar(toolbar)

        deepstreamFactory = DeepstreamFactory.getInstance()
        stateRegistry = StateRegistry.instance

        stateRegistry!!.gson = Gson()

        editPassword.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        emailSignInButton.setOnClickListener { attemptLogin() }
    }

    private fun attemptLogin() {
        if (authTask != null) {
            return
        }
        editEmail.error = null
        editPassword.error = null

        val email = editEmail.text.toString()
        val password = editPassword.text.toString()

        var cancel = false
        var focusView: View? = null

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editPassword.error = getString(R.string.error_invalid_password)
            focusView = editPassword
            cancel = true
        }

        if (TextUtils.isEmpty(email)) {
            editEmail.error = getString(R.string.error_field_required)
            focusView = editEmail
            cancel = true
        } else if (!isEmailValid(email)) {
            editEmail.error = getString(R.string.error_invalid_email)
            focusView = editEmail
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()
        } else {
            showProgress(true)
            authTask = UserLoginTask(email, password)
            authTask!!.execute(null as Void?)
        }
    }

    private fun isEmailValid(email: String): Boolean = email.contains("@")

    private fun isPasswordValid(password: String): Boolean = password.length > 4

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)

            loginForm.visibility = if (show) View.GONE else View.VISIBLE
            loginForm.animate().setDuration(shortAnimTime.toLong()).alpha(
                    (if (show) 0 else 1).toFloat()).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    loginForm.visibility = if (show) View.GONE else View.VISIBLE
                }
            })

            progressLogin.visibility = if (show) View.VISIBLE else View.GONE
            progressLogin.animate().setDuration(shortAnimTime.toLong()).alpha(
                    (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    progressLogin.visibility = if (show) View.VISIBLE else View.GONE
                }
            })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressLogin.visibility = if (show) View.VISIBLE else View.GONE
            loginForm.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    private fun createUserAccount(credentials: JsonObject) {
        Log.w("dsh", credentials.get("email").toString() + " not logged in, creating user account")

        val url: URL
        val conn: HttpURLConnection
        val writer: BufferedWriter

        try {
            val endpoint = "https://api.deepstreamhub.com/api/v1/user-auth/signup/" + getString(R.string.dsh_api_key)
            url = URL(endpoint)

            conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "POST"
            conn.setRequestProperty("Content-Type", "application/json")
            conn.doOutput = true
            conn.doInput = true

            writer = BufferedWriter(OutputStreamWriter(conn.outputStream))
            Log.w("dsh", "writing " + credentials.toString() + " to " + endpoint)
            writer.write(credentials.toString())
            writer.flush()

            val responseCode = conn.responseCode

            if (responseCode != 201) {
                Log.w("dsh", "error creating user")
                return
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    inner class UserLoginTask internal constructor(email: String, password: String) : AsyncTask<Void, Void, Boolean>() {

        private val credentials: JsonObject = JsonObject()

        init {
            credentials.addProperty("email", email)
            credentials.addProperty("type", "email")
            credentials.addProperty("password", password)
        }

        override fun doInBackground(vararg params: Void): Boolean? {

            var result: LoginResult

            val endpointUrl = getString(R.string.dsh_login_url)
            try {
                Log.w("dsh", "connecting client on url: " + endpointUrl)
                client = deepstreamFactory!!.getClient(endpointUrl)
                client!!.setRuntimeErrorHandler { topic, event, s -> Log.w("dsh", "Error:" + topic + event.toString() + s) }
            } catch (e1: URISyntaxException) {
                Log.w("dsh", "error connecting to dsh")
                return false
            }

            client!!.addConnectionChangeListener { connectionState -> Log.w("dsh", connectionState.toString()) }

            result = client!!.login(credentials)

            Log.w("dsh", result.data.toString() + " " + result.loggedIn())

            if (!result.loggedIn()) {
                createUserAccount(credentials)
                result = client!!.login(credentials)
            }
            val clientData = stateRegistry!!.gson?.toJsonTree(result.data) as JsonObject
            Log.w("dsh", "received client data " + clientData.toString())
            val userId = clientData.get("id").asString
            val email = credentials.get("email").asString

            stateRegistry!!.userId = userId
            stateRegistry!!.email = email

            val record = client!!.record.getRecord("users/" + userId)
            val user = UserDS(userId, email, true, record["score"].asInt)
            record.setMergeStrategy(MergeStrategy.REMOTE_WINS)
            record.set(stateRegistry!!.gson?.toJsonTree(user))

            val users = client!!.record.getList("users")
            users.entries = arrayOf()

            if (!Arrays.asList(*users.entries).contains("users/" + userId)) {
                users.addEntry("users/" + userId)
            }

            return true
        }

        override fun onPostExecute(success: Boolean?) {
            authTask = null
            showProgress(false)

            if (success!!) {
                val intent = Intent(this@DeepStreamLoginActivity, DeepStreamActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                editPassword.error = "An error occurred connecting to deepstream Hub"
                editPassword.requestFocus()
            }
        }

        override fun onCancelled() {
            authTask = null
            showProgress(false)
        }
    }
}

