package cz.marekhaubert.diploma.deepstream

import android.content.res.ColorStateList
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.util.Log
import cz.marekhaubert.diploma.*
import cz.marekhaubert.diploma.base.BaseActivity
import io.deepstream.*
import io.deepstream.List
import kotlinx.android.synthetic.main.activity_deepstream.*
import java.net.URISyntaxException
import java.util.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout


/**
 * Created by Marek Haubert on 04.09.2017.
 */
class DeepStreamActivity : BaseActivity() {

    private var factory: DeepstreamFactory = DeepstreamFactory.getInstance()
    private var client: DeepstreamClient? = null
    private var stateRegistry: StateRegistry = StateRegistry.instance
    private var users: LinkedHashMap<String, UserDS> = LinkedHashMap()
    private var userList: List? = null
    private var buttonChangedListener: ListEntryChangedListener? = null
    private var presenceEventListener: PresenceEventListener? = null
    private var connectedUsers: ArrayList<String>? = null
    private val buttonList = mutableMapOf<Int, Pair<ButtonItem, FloatingActionButton>>()
    private val random = Random(582)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deepstream)
        setSupportActionBar(toolbar)
        initDeepStream()

        layoutButtonPlaceholder.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                createButtonList()
                layoutButtonPlaceholder.viewTreeObserver.removeGlobalOnLayoutListener(this)
            }
        })
    }

    /**
     * Inicializuje klientské připojení k DeepStream databázi.
     */
    private fun initDeepStream() {
        try {
            client = factory.getClient(getString(R.string.dsh_login_url))
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }
        showInitUserScore()
        handleUserPresence()
    }

    /**
     * Vyčítá seznam tlačítek z DeepStream databáze pro zobrazení v UI aplikace.
     */
    private fun createButtonList() {
        sequenceOf(0..9).flatten().forEach {
            // vyčtení atributů tlačítka z databáze
            val buttonRecord = client!!.record.getRecord("Button/$it")
            // převod načtených atributů do formátu JSON
            val buttonJson = buttonRecord.get().asJsonObject
            // deserializace načtených atributů zprávy z formátu JSON na instanci Kotlin třídy
            val buttonItem = ButtonItem(
                    buttonJson.get("id").asInt,
                    buttonJson.get("width").asInt,
                    buttonJson.get("height").asInt,
                    buttonJson.get("active").asBoolean
            )
            initButton(buttonItem)
            // vytváří posluchač události pro realtime příjem změn dat tlačítek v DeepStream databází
            subscribeButtonList(buttonRecord)
        }
    }

    /**
     * Vytváří posluchač události pro realtime příjem změn dat tlačítek v DeepStream databází.
     */
    private fun subscribeButtonList(buttonRecord: Record) {
        buttonRecord.subscribe { string, jsonElement ->
            val buttonJsonSubscribe = jsonElement.asJsonObject
            val buttonId = buttonJsonSubscribe.get("id").asInt
            val buttonWidth = buttonJsonSubscribe.get("width").asInt
            val buttonHeight = buttonJsonSubscribe.get("height").asInt
            val buttonActive = buttonJsonSubscribe.get("active").asBoolean
            // aktualizace lokálně uloženého seznamu tlačítek
            buttonList[buttonId]?.first?.width = buttonWidth
            buttonList[buttonId]?.first?.height = buttonHeight
            buttonList[buttonId]?.first?.active = buttonActive
            // aktualizace souřadnic a zobrazení tlačítka na obrazovce
            runOnUiThread { buttonList[buttonId]?.let { updateButtonView(it) } }
        }
    }

    /**
     * Vytváří UI komponentu pro tlačítko.
     * @param buttonItem Datová instance tlačítka z DeepStream databáze.
     */
    private fun initButton(buttonItem: ButtonItem) {
        // vytvoření UI reprezentaci tlačítka
        val fab = FloatingActionButton(this)
        buttonList.put(buttonItem.id, Pair(buttonItem, fab))
        Log.w("Button", "addButton: ${buttonItem.id} ${buttonItem.width} ${buttonItem.height} ${buttonItem.active}")
        // grafická úprava tlačítka
        setUpButton(buttonList[buttonItem.id]!!)
    }

    /**
     * Provádí nastavení tlačítka.
     * @param fab Objekt s parametry tlačítka a jeho grafickou Android komponentou.
     */
    private fun setUpButton(fab: Pair<ButtonItem, FloatingActionButton>) {
        updateButtonView(fab)
        // vytvoření posluchače události kliknutí na tlačítko
        fab.second.setOnClickListener {
            val updateButton = client!!.record.getRecord("Button/${fab.first.id}")
            val buttonItem = ButtonItem(fab.first.id, random.nextInt(100), random.nextInt(100), fab.first.active)
            updateButton.set(stateRegistry.gson?.toJsonTree(buttonItem))
            updateUserScore(fab.first.active)
        }
        // přidání tlačítka do layoutu
        layoutButtonPlaceholder.addView(fab.second)
    }

    /**
     * Upravuje grafickou podobu tlačítka.
     * @param fab Objekt s parametry tlačítka a jeho grafickou Android komponentou.
     */
    private fun updateButtonView(fab: Pair<ButtonItem, FloatingActionButton>) {
        // nastavení souřadnic tlačítka
        changeFabCoordinates(fab.second, fab.first.width, fab.first.height)
        // nastavení vzhledu tlačítka
        fab.second.setImageDrawable(ContextCompat.getDrawable(this, if (fab.first.active) R.drawable.ic_add else R.drawable.ic_remove))
        fab.second.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, if (fab.first.active) R.color.colorAccent else R.color.colorPrimary))
        fab.second.layoutParams = FrameLayout.LayoutParams(250, 250)
    }

    /**
     * Zobrazuje v UI komponentě skóre aktuálně přihlášeného uživatele.
     */
    private fun showInitUserScore() {
        val userScoreRecord = client!!.record.getRecord("users/${stateRegistry.userId}")
        val score = userScoreRecord.get("score").asInt
        txtScore.text = getString(R.string.score_placeholder, score)
    }

    /**
     * Ukládá do databáze novou hodnotu skóre aktuálně přihlášeného uživatele.
     */
    private fun updateUserScore(isPlusPoint: Boolean) {
        val userScoreRecord = client!!.record.getRecord("users/${stateRegistry.userId}")
        var score = userScoreRecord.get("score").asInt
        score += if (isPlusPoint) 10 else -20
        userScoreRecord.set("score", score)
        txtScore.text = getString(R.string.score_placeholder, score)
    }

    /**
     * Nastavuje pozici tlačítka na základě dat z DeepStream databáze a také velikosti a proporcích displeje konkrétního mobilního zařízení.
     */
    private fun changeFabCoordinates(fab: FloatingActionButton, width: Int, height: Int) {
        val newXCoordinate = layoutButtonPlaceholder.width * (width.toFloat() / 100) - ((width.toFloat() / 100) * 250)
        val newYCoordinate = layoutButtonPlaceholder.height * (height.toFloat() / 100) - ((height.toFloat() / 100) * 250)
        fab.animate().x(newXCoordinate).y(newYCoordinate).duration = 300L
    }

    override fun onDestroy() {
        super.onDestroy()
        userList!!.unsubscribe(buttonChangedListener)
        client!!.presence.unsubscribe(presenceEventListener)
        userList!!.discard()
    }

    private fun addUser(id: String) {
        val userRecord = client!!.record.getRecord("users/" + id)
        val email = userRecord.get("email").asString
        val score = userRecord.get("score").asInt
        val online = connectedUsers!!.indexOf(id) != -1
        Log.d("User", "email:$email | online:$online")
        users.put(id, UserDS(id, email, online, score))
        userRecord.discard()
    }

    private fun handleUserPresence() {
        try {
            connectedUsers = ArrayList(Arrays.asList(*client!!.presence.all))
        } catch (deepstreamError: DeepstreamError) {
            deepstreamError.printStackTrace()
        }

        userList = client!!.record.getList("users")
        userList?.entries?.filter { it != "users/${stateRegistry.userId}" }?.forEach { addUser(it.substring(6)) }

        buttonChangedListener = object : ListEntryChangedListener {
            override fun onEntryAdded(listName: String, userId: String, position: Int) {
                runOnUiThread {
                    addUser(userId.substring(6))
                }
            }

            override fun onEntryRemoved(s: String, s1: String, i: Int) {}

            override fun onEntryMoved(s: String, s1: String, i: Int) {}
        }
        userList!!.subscribe(buttonChangedListener)

        presenceEventListener = object : PresenceEventListener {
            override fun onClientLogin(userId: String) {
                connectedUsers!!.add(userId)
                val user = users[userId] ?: return
                user.online = true
            }

            override fun onClientLogout(userId: String) {
                connectedUsers!!.remove(userId)
                val user = users[userId]
                user?.online = false
            }
        }
        client!!.presence.subscribe(presenceEventListener)
    }
}