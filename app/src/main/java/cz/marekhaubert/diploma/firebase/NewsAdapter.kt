package cz.marekhaubert.diploma.firebase

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.getFormattedTime
import kotlinx.android.synthetic.main.item_news.view.*

/**
 * Created by Marek Haubert on 19.08.2017.
 */
class NewsAdapter(val context: Context, private val items: List<News>, private val listener: (News) -> Unit) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_news, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(news: News, listener: (News) -> Unit) = with(itemView) {
            txtTitle.text = news.title
            txtPerex.text = news.perex
            txtTime.text = news.timestamp.getFormattedTime(context)
            Glide.with(context).load(news.imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imgNews)
            Glide.with(context).load(news.authorObject?.imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imgAvatar)
            txtUsername.text = news.authorObject?.name
            setOnClickListener { listener(news) }
        }

    }
}