package cz.marekhaubert.diploma.firebase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.androidhuman.rxfirebase2.database.data
import com.androidhuman.rxfirebase2.database.dataChanges
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.database.FirebaseDatabase
import cz.marekhaubert.diploma.NEWS_KEY
import cz.marekhaubert.diploma.NEWS_TITLE
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.getFormattedTime
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast
import timber.log.Timber

/**
 * Created by Marek Haubert on 20.08.2017.
 */
class DetailActivity : AppCompatActivity() {

    private val database: FirebaseDatabase? = FirebaseDatabase.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        toolbar.title = intent.getStringExtra(NEWS_TITLE)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        loadNewsItem()
    }

    /**
     * Načítá obsah jedné konkrétní novinky dle jejího ID.
     */
    private fun loadNewsItem() {
        // vyčtení identifikátoru novinky, která se má načíst, z objektu "intent", který přenáší data mezi jednotlivými obrazovkami a dalšími komponentami
        val newsKey = intent.getStringExtra(NEWS_KEY)
        // načtení konkrétní novinky z Firebase realtime databáze
        database?.getReference("news")?.child(newsKey)?.data()?.subscribe({ newsResponse ->
            val news = newsResponse.getValue(News::class.java)
            bindDataToViews(news!!)
            // načtení autora novinky
            database.getReference("author")?.child(news.author)?.dataChanges()?.subscribe(
                    { authorResponse -> showUserData((authorResponse.getValue(Author::class.java) as Author)) })
        }, {
            // v případě výskytu jakékoliv chyby během stahování dat je uživateli zobrazena chybová hláška
            toast(R.string.firebase_data_loading_error)
        })
    }

    /**
     * Vkládá načtená data novinky do příslušných UI prvků.
     */
    private fun bindDataToViews(news: News) {
        txtPerex.text = news.perex
        txtText.text = news.text
        txtTime.text = news.timestamp.getFormattedTime(this)
        Glide.with(this).load(news.imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imgNews)
    }

    /**
     * Vkládá načtená data autora do příslušných UI prvků.
     */
    private fun showUserData(author: Author) {
        Glide.with(this).load(author.imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imgAvatar)
        txtUsername.text = author.name
    }
}