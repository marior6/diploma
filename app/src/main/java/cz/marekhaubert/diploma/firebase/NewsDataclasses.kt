package cz.marekhaubert.diploma.firebase

import org.joda.time.DateTime

/**
 * Created by Marek Haubert on 19.08.2017.
 */

data class News(var id: String, var title: String, var perex: String, var text: String, var author: String, var authorObject: Author?, var timestamp: Long, var imageUrl: String, var category: String) {
    constructor() : this("", "", "", "", "", null, DateTime().millis / 1000L, "", "")
}

data class Category(var name: String, var color: String) {
    constructor() : this("", "")
}

data class Author(var name: String, var imageUrl: String) {
    constructor() : this("", "")
}