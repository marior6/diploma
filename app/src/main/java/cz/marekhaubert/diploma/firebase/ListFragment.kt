package cz.marekhaubert.diploma.firebase

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidhuman.rxfirebase2.database.dataChanges
import com.androidhuman.rxfirebase2.database.dataChangesOf
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import cz.marekhaubert.diploma.NEWS_KEY
import cz.marekhaubert.diploma.NEWS_TITLE
import cz.marekhaubert.diploma.R
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.support.v4.toast
import org.joda.time.DateTime
import timber.log.Timber

/**
 * Created by Marek Haubert on 19.08.2017.
 */
@SuppressLint("ValidFragment")
class ListFragment : Fragment() {

    private val database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    private val list: MutableList<News> = mutableListOf()
    private var categoryId by FragmentArgumentDelegate<String>()

    companion object {
        fun newInstance(categoryId: String) = ListFragment().apply {
            this.categoryId = categoryId
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleProgress(true)
        recyclerFirebase.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerFirebase.adapter = NewsAdapter(activity, list) { news ->
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra(NEWS_KEY, news.id)
            intent.putExtra(NEWS_TITLE, news.title)
            startActivity(intent)
        }

        loadData()
    }

    /*// Datová třída novinek.
    data class News(var id: String, var title: String, var perex: String, var text: String, var author: String, var authorObject: Author?, var timestamp: Long, var imageUrl: String, var category: String) {
        // defaultní konstruktor třídy
        constructor() : this("", "", "", "", "", null, DateTime().millis / 1000L, "", "")
    }

    //Datová třída autora novinky.
    data class Author(var name: String, var imageUrl: String) {
        // defaultní konstruktor třídy
        constructor() : this("", "")
    }*/

    /**
     * Načítá seznam novinek z Firebase Realtime databáze a ke každé stažené novince následně načítá data o příslušném autorovi novinky.
     */
    private fun loadData() {
        // definice struktury dat, které jsou očekávány v odpovědi z Firebase Database
        val simpleGenericTypeIndicator = object : GenericTypeIndicator<Map<String, @JvmSuppressWildcards News>>() {}

        // načtení novinek pro konkrétní kategorii s identifikátorem @categoryId z Firebase realtime databáze
        database?.getReference("news")?.orderByChild("category")?.equalTo("category$categoryId")?.dataChangesOf(simpleGenericTypeIndicator)?.subscribe({ newsMap ->
            // odstranění aktuálně zobrazených dat na aktuální stránce
            clearAdapterData()
            // cyklus pro načtení autora pro každou načtenou novinku
            newsMap.get().entries.forEach { news ->
                database.getReference("author")?.child(news.value.author)?.dataChangesOf<Author>()?.subscribe({ author ->
                    news.value.id = news.key
                    // uložení autora do příslušného objektu novinky
                    news.value.authorObject = author.get()
                    // vložení novinky do seznamu novinek
                    list.add(news.value)
                    // informování adaptéru (grafického zobrazení seznamu novinek), aby zobrazil načtenou novinku
                    recyclerFirebase?.adapter?.notifyItemInserted(if (list.size > 0) list.size else 0)
                    // pokyn ke znevidilnění signalizace načítání dat a zobrazení načteného obsahu
                    handleProgress(false)
                })
            }
        }, {
            // v případě výskytu jakékoliv chyby během stahování dat je uživateli vizuálně ukončeno načítání dat
            handleProgress(false)
            // uživateli je zobrazena chybová hláška
            toast(R.string.firebase_data_loading_error)
        })
    }

    private fun clearAdapterData() {
        list.clear()
        recyclerFirebase?.adapter?.notifyDataSetChanged()
    }

    private fun handleProgress(progressVisible: Boolean) {
        progressList.visibility = if (progressVisible) View.VISIBLE else View.GONE
        recyclerFirebase.visibility = if (progressVisible) View.GONE else View.VISIBLE
    }

}