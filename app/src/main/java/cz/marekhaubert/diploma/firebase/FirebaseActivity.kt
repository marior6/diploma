package cz.marekhaubert.diploma.firebase

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.androidhuman.rxfirebase2.database.dataChangesOf
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import cz.marekhaubert.diploma.R
import cz.marekhaubert.diploma.base.BaseActivity
import cz.marekhaubert.diploma.populateFirebaseDatabase
import kotlinx.android.synthetic.main.activity_firebase.*
import timber.log.Timber


class FirebaseActivity : BaseActivity() {

    private val database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    private val categoryGenericTypeIndicator = object : GenericTypeIndicator<Map<String, @JvmSuppressWildcards Category>>() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase)
        setSupportActionBar(toolbar)

        database?.getReference("category")?.dataChangesOf(categoryGenericTypeIndicator)?.subscribe({
            viewPager.adapter = SectionPagerAdapter(supportFragmentManager, it.get())
            tabLayout.setupWithViewPager(viewPager)
        }, {
            Timber.e(it, "Could not load categories")
        })

        //fabSend.setOnClickListener { populateFirebaseDatabase(database) }
    }

    inner class SectionPagerAdapter(fm: FragmentManager, private var categoryMap: Map<String, @JvmSuppressWildcards Category>) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = ListFragment.newInstance(position.toString())
        override fun getCount(): Int = categoryMap.size
        override fun getPageTitle(position: Int): CharSequence = categoryMap["category$position"]?.name!!
    }
}
